import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { InfluencerController } from './influencer/influencer.controller';
import { InfluencerService } from './influencer/influencer.service';
import { InfluencerModule } from './influencer/influencer.module';
import { StoreController } from './store/store.controller';
import { StoreService } from './store/store.service';
import { StoreModule } from './store/store.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/test'),
    InfluencerModule,
    StoreModule],
  controllers: [AppController],
  providers: [AppService],
  
})
export class AppModule {}
