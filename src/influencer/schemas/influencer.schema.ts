import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type InfluencerDocument = Influencer & Document;

@Schema()
export class Influencer {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  bio: string;

  @Prop()
  image?: string;

  @Prop({required: true})
  storeName: string;

  @Prop()
  completedAt?: Date;

  @Prop({ required: true })
  createdAt: Date;

  @Prop()
  deletedAt?: Date;
}

export const InfluencerSchema = SchemaFactory.createForClass(Influencer);
