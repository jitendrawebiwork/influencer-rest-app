export class BaseInfluencerDto {
    name: string
    bio: string
    image: string
    storeName: string
 }
