import { BaseInfluencerDto } from './base-influencer.dto';

export class UpdateInfluencerDto extends BaseInfluencerDto {
  completedAt: Date;
}
