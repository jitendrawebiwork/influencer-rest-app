import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Influencer, InfluencerDocument } from './schemas/influencer.schema';
import { CreateInfluencerDto } from './dto/create-influencer.dto';
import { UpdateInfluencerDto } from './dto/update-influencer.dto';

@Injectable()
export class InfluencerService {
    constructor(@InjectModel(Influencer.name) private readonly model: Model<InfluencerDocument>) {}

    async findAll(): Promise<Influencer[]> {
        return await this.model.find().exec();
      }
    
      async findOne(id: string): Promise<Influencer> {
        return await this.model.findById(id).exec();
      }
    
      async create(createInfluencerDto: CreateInfluencerDto): Promise<Influencer> {
        return await new this.model({
          ...createInfluencerDto,
          createdAt: new Date(),
        }).save();
      }
    
      async update(id: string, updateInfluencerDto: UpdateInfluencerDto): Promise<Influencer> {
        return await this.model.findByIdAndUpdate(id, updateInfluencerDto).exec();
      }
    
      async delete(id: string): Promise<Influencer> {
        return await this.model.findByIdAndDelete(id).exec();
      }

}
