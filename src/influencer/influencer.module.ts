import { Module } from '@nestjs/common';
import { InfluencerService } from './influencer.service';
import { InfluencerController } from './influencer.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Influencer, InfluencerSchema } from './schemas/influencer.schema';


@Module({
    providers: [InfluencerService],
    controllers: [InfluencerController],
    imports: [
        MongooseModule.forFeature([{ name: Influencer.name, schema: InfluencerSchema }]),
  ],

})
export class InfluencerModule {}
