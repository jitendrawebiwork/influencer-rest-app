import { Controller, Get, Param, Post, Put, Body,Delete } from '@nestjs/common';
import { InfluencerService } from './influencer.service';
import { CreateInfluencerDto } from './dto/create-influencer.dto';
import { UpdateInfluencerDto } from './dto/update-influencer.dto';


@Controller('influencer')
export class InfluencerController {
    constructor(private readonly service: InfluencerService) {}

  @Get()
  async index() {
    return await this.service.findAll();
  }

  @Get(':id')
  async find(@Param('id') id: string) {
    return await this.service.findOne(id);
  }

  @Post()
  async create(@Body() createInfluencerDto: CreateInfluencerDto) {
    return await this.service.create(createInfluencerDto);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() updateInfluencerDto: UpdateInfluencerDto) {
    return await this.service.update(id, updateInfluencerDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    return await this.service.delete(id);
  }


}
