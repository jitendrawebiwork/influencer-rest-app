import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Store, StoreDocument } from './schemas/store.schema';
import { CreateStoreDto } from './dto/create-store.dto';
import { UpdateStoreDto } from './dto/update-store.dto';

@Injectable()
export class StoreService {
    constructor(@InjectModel(Store.name) private readonly model: Model<StoreDocument>) {}

    async findAll(): Promise<Store[]> {
        return await this.model.find().exec();
      }
    
      async findOne(id: string): Promise<Store> {
        return await this.model.findById(id).exec();
      }
    
      async create(createStoreDto: CreateStoreDto): Promise<Store> {
        return await new this.model({
          ...createStoreDto,
          createdAt: new Date(),
        }).save();
      }
    
      async update(id: string, updateStoreDto: UpdateStoreDto): Promise<Store> {
        return await this.model.findByIdAndUpdate(id, updateStoreDto).exec();
      }
    
      async delete(id: string): Promise<Store> {
        return await this.model.findByIdAndDelete(id).exec();
      }
}
