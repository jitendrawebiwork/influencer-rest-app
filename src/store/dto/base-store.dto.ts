export class BaseStoreDto {
    storeEmail: string
    storeId: string
    storeUrl: string
    storeName: string
}