import { BaseStoreDto } from './base-store.dto';

export class UpdateStoreDto extends BaseStoreDto {
  completedAt: Date;
}
