import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { StoreService } from './store.service';
import { CreateStoreDto } from './dto/create-store.dto';
import { UpdateStoreDto } from './dto/update-store.dto';

@Controller('store')
export class StoreController {
    constructor(private readonly service: StoreService) { }

    @Get()
    async index() {
        return await this.service.findAll();
    }

    @Get(':id')
    async find(@Param('id') id: string) {
        return await this.service.findOne(id);
    }

    @Post()
    async create(@Body() createStoreDto: CreateStoreDto) {
        return await this.service.create(createStoreDto);
    }

    @Put(':id')
    async update(@Param('id') id: string, @Body() updateStoreDto: UpdateStoreDto) {
        return await this.service.update(id, updateStoreDto);
    }

    @Delete(':id')
    async delete(@Param('id') id: string) {
        return await this.service.delete(id);
    }

}
