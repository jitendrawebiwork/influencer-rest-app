import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type StoreDocument = Store & Document;

@Schema()
export class Store {
  @Prop({ required: true })
  storeEmail: string;

  @Prop({ required: true })
  storeId: string;

  @Prop({ required: true })
  storeUrl: string;

  @Prop({required: true})
  storeName: string;

  @Prop()
  completedAt?: Date;

  @Prop({ required: true })
  createdAt: Date;

  @Prop()
  deletedAt?: Date;
}

export const StoreSchema = SchemaFactory.createForClass(Store);